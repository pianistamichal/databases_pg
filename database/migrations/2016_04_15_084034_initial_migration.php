<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->string("first_name");
            $table->string("last_name");
            $table->date("born_date");
            $table->integer('pesel');
            $table->integer("phone_number");
        });

        Schema::create('doctor', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unsigned();
            $table->string("treatment_available")->nullable()->default(null);

            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });

        Schema::create('patient', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });

        Schema::create('timetable', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('doctor_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->date("treatment_date");
            $table->string("information")->nullable()->default(null);

            $table->foreign('doctor_id')->references('id')->on('doctor')->onDelete('cascade');
            $table->foreign('patient_id')->references('id')->on('patient')->onDelete('cascade');
        });

        Schema::create('treatment', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('doctor_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->string("treatment_type")->nullable()->default(null);
            $table->string("information")->nullable()->default(null);
            $table->date("treatment_date_start");
            $table->date("treatment_date_end");

            $table->foreign('doctor_id')->references('id')->on('doctor')->onDelete('cascade');
            $table->foreign('patient_id')->references('id')->on('patient')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
        Schema::drop('doctor');
        Schema::drop('patient');
        Schema::drop('timetable');
        Schema::drop('treatment');
    }
}
