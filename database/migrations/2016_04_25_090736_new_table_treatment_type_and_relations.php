<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableTreatmentTypeAndRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatment_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
        });

        Schema::table('treatment', function (Blueprint $table) {
            $table->integer('treatment_type')->unsigned()->change();
            $table->foreign('treatment_type')->references('id')->on('treatment_type')->onDelete('cascade');
        });

        Schema::table('doctor', function (Blueprint $table) {
            $table->integer('treatment_available')->unsigned()->change();
            $table->foreign('treatment_available')->references('id')->on('treatment_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
