/**
 * Created by pianistamichal on 26.04.16.
 */
$(document).ready(function() {
    var x = 10;
    var y = 30;

    $('li:not(li.not-window) a ').click(function(event) {
        x += 10;
        y += 10;
        event.preventDefault();
        var href = $(this).attr('href');
        $.ajax({
            url: href,
            success: function($view) {
                $('.to-append').append($view).children().last().addClass('placeddiv').css({
                    left: x,
                    top: y,
                    position: 'absolute'
                });
                $('.modal-dialog').draggable({containment: 'html',  snap: true }).resizable({containment: 'html'}).mousedown(function() {
                    $(this).topZIndex();
                });
                $('.ui-resizable-handle').css('z-index', 'auto');


                $('.close').click(function(event) {
                    event.preventDefault();
                    $(this).parent().parent().parent().remove();
                });
            }
        });
    });

});
