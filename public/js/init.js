/**
 * Created by pianistamichal on 28.04.16.
 */
$(document).ready(function() {
    $.ajax({
        url: '/basic',
        success: function($view) {
            $('.to-append').append($view).children().last().addClass('placeddiv').css({
                left: 10,
                top: 30,
                position: 'absolute'
            });
            $('.modal-dialog').draggable({containment: 'html',  snap: true }).resizable({containment: 'html'});
            $('.close').click(function(event) {
                event.preventDefault();
                $(this).parent().parent().parent().remove();
            });
        }
    });
});