$(document).ready(function() {
    $('.search-input').keyup(function() {
        $data = $(this).val();
        if($data.length == 0) {
            $('.live-search').html('');
        } else {
            $.ajax({
                url: '/profile/search/' + $data,
                success: function($view) {
                    $('.live-search').html($view);
                }
            });
        }
    });
});