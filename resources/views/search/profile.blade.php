<table class="table">
    <thead class="thead-inverse">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Pesel</th>
    </tr>
    </thead>
    <tbody>
        @foreach($results as $result)
            <tr>
                <td>{{ $result->first_name }}</td>
                <td>{{ $result->last_name }}</td>
                <td>{{ $result->pesel }}</td>
            </tr>
        @endforeach
    </tbody>
</table>