@extends('layouts.single', array('title' => 'About'))

@section('content')
    <div class="col-md-8">
        Aktywnie rozwijam swoje umiejętności programistyczne w javie, na projekty uczelniane jak również w czasie godzin pracy, natomiast drugą z rzeczy którą obecnie robię dla własnej kariery to praca na rzecz Politechniki Gdańskiej współtworząc koło marketingowe. W wolnym czasie pasjonat poszerzania świadomości, słuchania najróżniejszej muzyki, pisania wierszy oraz spotykania się ze znajomymi, a to jedynie niektóre z moich zainteresowań.

        Founder in http://www.ontalk.pl

        Po dodaniu mnie do kontaktów(co z uprzejmością proponuję) poniżej odnajdą państwo pomocne informacje jak np CV. Jednakże obecnie nie poszukuję pracy, gdyż mam dużo pracy z powyższymi i nie tylko. Gdy tylko to się zmieni uaktualnie profil na linkedinie.
    </div>
    <div class="col-md-4">
        <img src="{{ URL::asset('/img/me.jpg') }}" class="img-thumbnail" alt="Michał Błociński">
    </div>
@endsection
