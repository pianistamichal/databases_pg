@extends('layouts.single', array('title' => 'Welcome'))

@section('content')
    <div class="col-md-12">
        <h1>Clinic project - database</h1>
    </div>

    <div class="col-md-12">
        <p>We are glad to see You again!</p>
    </div>

    <div class="col-md-8">
        <p>A clinic (or outpatient clinic or ambulatory care clinic) is a healthcare facility that is primarily devoted to the care of outpatients. Clinics can be privately operated or publicly managed and funded, and typically cover the primary healthcare needs of populations in local communities, in contrast to larger hospitals which offer specialised treatments and admit inpatients for overnight stays. Most commonly, the word clinic in English refers to a general medical practice, run by one or several general practitioners, or a specialist clinic. Some clinics grow to be institutions as large as major hospitals, or become associated with a hospital or medical school, while retaining the name “clinic."</p>
    </div>
    <div class="col-md-4">
        <img src="{{ URL::asset('/img/clinic.jpg') }}" class="img-thumbnail" alt="Cinque Terre">
    </div>
@stop