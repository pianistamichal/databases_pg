@extends('layouts.single', array('title' => 'Dashboard'))

<script src="{{ URL::asset('js/search_profile.js') }}"></script>

@section('content')
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Search for profile</label>

        <div class="col-md-8">
            <input type="text" class="form-control search-input" name="name">
        </div>
    </div>
    <div class="live-search col-md-12">

    </div>
@endsection
