@extends('layouts.single', ['title' => 'Create new profile'])

@section('content')
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/create') }}">
        {!! csrf_field() !!}

        <div class="form-group{{ $errors->has('profile_type') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Profile type*</label>
            <div class="col-md-3">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="profile_type" value="patient" {{ old('profile_type') == 'patient' ? ' checked = "checked" ' : ''}}> Patient
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="profile_type" value="doctor" {{ old('profile_type') == 'doctor' ? ' checked = "checked" ' : ''}}> Doctor
                    </label>
                </div>
            </div>

            @if ($errors->has('profile_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('profile_type') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Name*</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">First name*</label>

            <div class="col-md-6">
                <input type="first_name" class="form-control" name="first_name" value="{{ old('first_name') }}">

                @if ($errors->has('first_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Last name*</label>

            <div class="col-md-6">
                <input type="last_name" class="form-control" name="last_name" value="{{ old('last_name') }}">

                @if ($errors->has('last_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('pesel') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Pesel*</label>

            <div class="col-md-6">
                <input type="pesel" class="form-control" name="pesel" value="{{ old('pesel') }}">

                @if ($errors->has('pesel'))
                    <span class="help-block">
                    <strong>{{ $errors->first('pesel') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Phone number</label>

            <div class="col-md-6">
                <input type="phone_number" class="form-control" name="phone_number" value="{{ old('phone_number') }}">

                @if ($errors->has('phone_number'))
                    <span class="help-block">
                    <strong>{{ $errors->first('phone_number') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>Register
                </button>
            </div>
        </div>
    </form>
@endsection