<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ URL::asset('js/click_button.js') }}"></script>
<script src="{{ URL::asset('js/init.js') }}"></script>
<script src="{{ URL::asset('js/jquery.topzindex.js') }}"></script>

@yield('javascript')