<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.header')

    <div class="container-fluid">
        <div class="row to-append">
            @if(Session::has('success'))
                <div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ Session::get('success') }}
                </div>
            @endif
        </div>
    </div>

    @include('includes.footer')
    @include('includes.javascript')

</body>
</html>
