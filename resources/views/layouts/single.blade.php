<div id="myModal" class="modal-dialog ui-resizable ui-draggable">
    <div class="modal-content" style="height: 100%; overflow: auto;">
        <div class="modal-header">
            <button type="button" class="close">
                &times;
            </button>
            <h4 class="modal-title">{{$title}}</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>