@extends('layouts.single', array('title' => 'Login'))

@section('content')
    <div class="well">
        <form id="loginForm" method="POST" action="{{ url('/login') }}" novalidate="novalidate">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Username</label>
                <input type="text" class="form-control" id="name" name="name" value="" required="" title="Please enter you username" placeholder="mkowalski">
                <span class="help-block">
                    @if ($errors->has('name'))
                        <strong>{{ $errors->first('name') }}</strong>
                    @endif
                </span>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>
                <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
                <span class="help-block">
                    @if ($errors->has('password'))
                        <strong>{{ $errors->first('password') }}</strong>
                    @endif
                </span>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" id="remember"> Remember login
                </label>
                <p class="help-block">(if this is a private computer)</p>
            </div>
            <button type="submit" class="btn btn-success btn-block">Login</button>
            <a href="{{ url('/password/reset') }}" class="btn btn-default btn-block">Forgot password?</a>
        </form>
    </div>
@endsection
