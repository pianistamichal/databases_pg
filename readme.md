# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


MySQL config:


```
#!php

user : root
pass : 
```


Apache virtual host config(directory can change):
When ServerName specified have to override vhost system file and change document root/directory

```
#!php                       

<VirtualHost *:80>
        ServerName ClinicProject.com

        ServerAdmin webmaster@localhost
        DocumentRoot /home/pianistamichal/programming/php/ClinicProject/public

        <Directory "/home/pianistamichal/programming/php/ClinicProject/public">
                AllowOverride all
                Require all granted
        </Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>


```


Baby steps(linux):

```
#!php

sudo git clone https://pianistamichal@bitbucket.org/pianistamichal/databases_pg.git .
```
```
#!php

sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
```

(need installer composer before this step)
```
#!php

sudo composer install
```

```
#!php

sudo php artisan migrate
```

This is all! Have fun!