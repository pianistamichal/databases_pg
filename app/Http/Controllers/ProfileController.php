<?php
/**
 * Created by PhpStorm.
 * User: pianistamichal
 * Date: 22.04.16
 * Time: 12:11
 */

namespace App\Http\Controllers;

use App\Doctor;
use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;
use App\User;

class ProfileController extends Controller
{
    /**
     * Create a new profile controller
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate() {
        return view('profile.create');
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request) {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->createUser($request->all());

        if($request->profile_type == 'patient') {
            $this->createPatient($user->id);
        } else if($request->profile_type == 'doctor') {
            $this->createDoctor($user->id);
        }

        Session::flash('success', 'I have created new profile ' . $request->name . ' with pesel password. Inform person to change it soon.');
        return redirect('/profile/create');
    }

    public function createDoctor($id) {
        $doctor = new Doctor();
        $doctor->user_id = $id;
        $doctor->save();
    }

    public function createPatient($id) {
        $patient = new Patient();
        $patient->user_id = $id;
        $patient->save();
    }

    public function createUser($data) {
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->phone_number = $data['phone_number'];
        $user->pesel = $data['pesel'];
        $user->password = bcrypt($data['pesel']);
        $user->save();
        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:user',
            'email' => 'email|max:255|unique:user',
            'pesel' => 'required|numeric|digits:11',
            'first_name' => 'required',
            'last_name' => 'required',
            'profile_type' => 'required',
        ]);
    }

}