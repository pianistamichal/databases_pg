<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.app');
    }

    public function basic() {
        if(Auth::check()) {
            return view('home.dashboard');
        } else {
            return view('home.welcome');
        }
    }

    /**
     * Show the application about site.
     *
     * @return \Illuminate\Http\Response
     */
    public function about() {
        return view('home.about');
    }
}
