<?php
namespace App\Http\Controllers;
use App\User;

/**
 * Created by PhpStorm.
 * User: pianistamichal
 * Date: 25.04.16
 * Time: 13:04
 */
class SearchController extends Controller
{
    public function searchProfile($text) {
        $user = new User();

        if(is_numeric($text)) {
            $pesel = intval($text);
            $results = $user->where('pesel', 'like', $pesel . '%')->get();
        } else {
            $splitted = explode(" ", $text);
            if(count($splitted) == 1) {
                $results = $user
                    ->where('first_name', 'like', '%'.$splitted[0].'%')
                    ->orWhere('last_name', 'like', '%'.$splitted[0].'%');
            } else {
                $results = $user
                    ->where(function($query) use($splitted) {
                        return $query->where('first_name', 'like', '%'.$splitted[0].'%')
                            ->where('last_name', 'like', '%'.$splitted[1].'%');
                    })
                    ->orWhere(function($query) use($splitted) {
                        return $query->where('first_name', 'like', '%'.$splitted[1].'%')
                            ->where('last_name', 'like', '%'.$splitted[0].'%');
                    });
            }
            $results = $results->get();
        }

        app('debugbar')->disable();

        return view('search.profile', ['results' => $results]);
    }
}