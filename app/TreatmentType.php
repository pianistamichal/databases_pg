<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'treatment_type';
    public $timestamps = false;
}