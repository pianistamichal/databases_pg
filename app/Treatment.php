<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: pianistamichal
 * Date: 16.04.16
 * Time: 16:24
 */
class Treatment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'treatment';
    public $timestamps = false;

    public function doctor_id()
    {
        return $this->belongsTo('App\Doctor');
    }

    public function patient_id()
    {
        return $this->belongsTo('App\Patient');
    }

    public function treatment_type() {
        return $this->hasMany('App\TreatmentType');
    }
}