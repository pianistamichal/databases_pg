<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: pianistamichal
 * Date: 16.04.16
 * Time: 16:24
 */
class Doctor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doctor';
    public $timestamps = false;

    public function user_id()
    {
        return $this->hasOne('App\User');
    }

    public function treatment_available() {
        return $this->hasMany('App\TreatmentType');
    }
}